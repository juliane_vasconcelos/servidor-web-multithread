package server;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;

public class Config {

	private final String CONFIG = "./config.txt";
	private final String DELIMITADOR = ";";
	private boolean liberarAcessoRestrito;
	private String login;
	private String senha;
	private String paginaSolicitante;

	public void leConfig() {
		try {
			@SuppressWarnings("resource")
			Scanner scanner = new Scanner(new FileReader(CONFIG)).useDelimiter(DELIMITADOR);
			int i = 0;
			while (i < 4) {
				String libera = scanner.next();
				libera = libera.substring(libera.lastIndexOf("=") + 1);
				if (libera.contains("true")) {
					liberarAcessoRestrito = true;
				} else {
					liberarAcessoRestrito = false;
				}
				i++;
				login = scanner.next();
				login = login.substring(login.lastIndexOf("=") + 1);
				i++;
				senha = scanner.next();
				senha = senha.substring(senha.lastIndexOf("=") + 1);
				i++;
				paginaSolicitante = scanner.next();
				paginaSolicitante = paginaSolicitante.substring(paginaSolicitante.lastIndexOf("=") + 1);
				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void atualizaConfig() {
		try {
			FileWriter arq = new FileWriter(CONFIG);
			PrintWriter gravarArq = new PrintWriter(arq);
			gravarArq.printf("liberarAcessoRestrito=" + liberarAcessoRestrito + ";\n");
			gravarArq.printf("login=" + login + ";\n");
			gravarArq.printf("senha=" + senha + ";\n");
			gravarArq.printf("paginaSolicitante=" + paginaSolicitante + ";\n");
			arq.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean isLiberarAcessoRestrito() {
		return liberarAcessoRestrito;
	}

	public void setLiberarAcessoRestrito(boolean liberarAcessoRestrito) {
		this.liberarAcessoRestrito = liberarAcessoRestrito;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getPaginaSolicitante() {
		return paginaSolicitante;
	}

	public void setPaginaSolicitante(String paginaSolicitante) {
		this.paginaSolicitante = paginaSolicitante;
	}

}