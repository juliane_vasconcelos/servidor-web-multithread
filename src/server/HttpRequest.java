package server;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class HttpRequest implements Runnable {

	final static String CRLF = "\r\n";
	Socket socket;
	ServerLog log;
	Config config;
	String login;
	String senha;

	public HttpRequest(Socket socket) {
		this.socket = socket;
	}

	// Implementar o método run() da interface Runnable.
	@Override
	public void run() {
		try {
			log = new ServerLog();
			log.startLog();
			config = new Config();
			config.leConfig();
			processRequest();
			log.finishLog();
		} catch (Exception e) {
			// caso ocorra erro generico, retorna a pagina index.html
			System.out.println(e);
			log = new ServerLog();
			log.startLog();
			criarEEnviarResposta(new DataOutputStream(getOutputStrem()), "/index.html");
			log.finishLog();
		}
	}

	private void processRequest() throws Exception {
		// Obter uma referÃªncia para os trechos de entrada e saÃ­da do socket.
		// stream de leitura
		InputStream inputStream = socket.getInputStream();

		// stream de escrita
		DataOutputStream dataOutputStream = new DataOutputStream(getOutputStrem());

		// Ajustar os filtros do trecho de entrada.
		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

		// Obter a linha de requisição da mensagem de requisição HTTP.
		String linhaRequisicao = obterLinhaRequisicaoHTTP(bufferedReader);

		// Obter e exibir as linhas de cabeçalho.
		obterLinhasDeCabecalho(bufferedReader, linhaRequisicao);

		String nomeArquivo = obterNomeArquivo(linhaRequisicao);

		if (isDirectory(nomeArquivo)) {
			criarEEnviarDiretorio(dataOutputStream, nomeArquivo);
			// autenticarAcesso(dataOutputStream, nomeArquivo);
		} else {
			criarEEnviarResposta(dataOutputStream, nomeArquivo);
		}

		// fechando os filtros de entrada e o socket
		inputStreamReader.close();
		bufferedReader.close();
		socket.close();
	}

	private OutputStream getOutputStrem() {
		try {
			return socket.getOutputStream();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void autenticarAcesso(DataOutputStream dataOutputStream, String nomeArquivo) throws IOException {
		// String statusLine, contentTypeLine, resposta;
		String statusLine = "HTTP/1.0 401 Unauthorized" + CRLF;
		dataOutputStream.writeBytes(statusLine);
		// https://httpstatuses.com/401
		// https://tools.ietf.org/html/rfc7235#section-4.1
		// https://tools.ietf.org/html/rfc7235#section-4.2
	}

	private void criarEEnviarDiretorio(DataOutputStream dataOutputStream, String nomeArquivo) throws IOException {
		boolean permission = true, exists = true;
		String statusLine = "";
		String contentTypeLine = "";
		String resposta = "";
		int numberOfBytes = 0; // numero de bytes enviados para gravar no log
		boolean processaForm = false;

		if (nomeArquivo.contains("autenticar")) {
			processaForm = true;
		}

		if (processaForm) {
			String login = this.login;
			String senha = this.senha;

			if (login.equals(config.getLogin()) && senha.equals(config.getSenha())) {
				nomeArquivo = config.getPaginaSolicitante();

				//atualizar o acess.txt do diretorio para nao ter que autenticar de novo
				FileWriter arq = new FileWriter(nomeArquivo + "/access.txt");
				PrintWriter gravarArq = new PrintWriter(arq);
				gravarArq.printf("login=" + login + ";\n");
				gravarArq.printf("senha=" + senha + ";");
				arq.close();
			} else {
				nomeArquivo = "./logininvalido.html";
				criarEEnviarResposta(dataOutputStream, nomeArquivo);
				return;
			}

		} else {
			// verifica se o diretorio existe
			File diretorio = new File(nomeArquivo);
			if (!diretorio.exists()) {
				exists = false;
			}
		}

		if (exists) {
			// verifica permissao de acesso ao diretorio
			permission = verificaSeAcessoExiste(nomeArquivo, permission);

			// caso exista a permissao
			if (permission) {
				File folder = new File(nomeArquivo);
				File[] listOfFiles = folder.listFiles();

				resposta = "<HTML><TITLE>List of " + nomeArquivo.replace(".", "") + "</TITLE><BODY>";

				for (int i = 0; i < listOfFiles.length; i++) {
					if (listOfFiles[i].isFile()) {
						resposta = resposta + "File " + listOfFiles[i].getName() + "</BR>";
					} else if (listOfFiles[i].isDirectory()) {
						resposta = resposta + "Directory " + listOfFiles[i].getName() + "</BR>";
					}
				}
				resposta = resposta + "</BODY></HTML>";

				statusLine = "HTTP/1.0 200 Ok" + CRLF;
				contentTypeLine = "Content-type: " + "text/html" + CRLF;
			} else {// nao existe a permissão
				System.out.println(config.isLiberarAcessoRestrito());
				if (config.isLiberarAcessoRestrito()) {
					statusLine = "HTTP/1.0 401 Unauthorized" + CRLF;
					config.setPaginaSolicitante(nomeArquivo);
					config.atualizaConfig();

					if (isDirectory(nomeArquivo)) {
						nomeArquivo = nomeArquivo + "/login.html";
					} else {
						String dir = nomeArquivo.substring(0, nomeArquivo.lastIndexOf("/"));
						nomeArquivo = dir + "/login.html";
					}
					criarEEnviarResposta(dataOutputStream, nomeArquivo, statusLine);
					return;
				} else {
					resposta = "<HTML><TITLE>Access of " + nomeArquivo.replace(".", "") + " denied</TITLE><BODY>"
							+ "Você não tem acesso a este diretorio.</BODY></HTML>";
					statusLine = "HTTP/1.0 403 Forbidden" + CRLF;
					contentTypeLine = "Content-type: " + "text/html" + CRLF;
				}
			}
		} else {// diretorio nao existe
			statusLine = "HTTP/1.0 404 Not Found" + CRLF;
			contentTypeLine = "Content-type: " + //
					"text/html"//
					+ CRLF;
			resposta = "<HTML>" + "<HEAD><TITLE>Not Found</TITLE></HEAD>"
					+ "<BODY> Ops! Pasta nao encontrada! Tente outra URL =) </BODY></HTML>";
		}

		// Enviar a linha de status.
		dataOutputStream.writeBytes(statusLine);
		numberOfBytes = numberOfBytes + statusLine.getBytes().length;

		// Enviar a linha de tipo de conteÃºdo.
		dataOutputStream.writeBytes(contentTypeLine);
		numberOfBytes = numberOfBytes + contentTypeLine.getBytes().length;

		// Enviar uma linha em branco para indicar o fim das linhas de
		// cabeçalho.
		dataOutputStream.writeBytes(CRLF);
		numberOfBytes = numberOfBytes + CRLF.getBytes().length;

		dataOutputStream.writeBytes(resposta);
		numberOfBytes = numberOfBytes + resposta.getBytes().length;

		// Grava o log da transação
		log.printLog(socket.getInetAddress().getHostAddress(), socket.getPort(), nomeArquivo, numberOfBytes);
	}

	private boolean verificaSeAcessoExiste(String nomeArquivo, boolean permission) {
		@SuppressWarnings("unused")
		FileInputStream fileInputStream = null;
		if (nomeArquivo.contains("login.html")) {
			return true;
		}
		if (isDirectory(nomeArquivo)) {
			try {
				// verificar se no diretorio ha um arquivo chamado access.txt
				fileInputStream = new FileInputStream(nomeArquivo + "/access.txt");
				@SuppressWarnings("resource")
				Scanner scanner = new Scanner(new FileReader(nomeArquivo + "/access.txt")).useDelimiter(";");

				String login = "";
				String senha = "";

				int i = 0;
				while (i < 2) {
					login = scanner.next();
					login = login.substring(login.lastIndexOf("=") + 1);
					i++;
					senha = scanner.next();
					senha = senha.substring(senha.lastIndexOf("=") + 1);
					i++;
				}

				if (login.equals(config.getLogin()) && senha.equals(config.getSenha())) {
					System.out.println("acesso correto");
					permission = true;
				} else {
					System.out.println("acesso INcorreto");
					permission = false;
				}
			} catch (FileNotFoundException e) {
				// Access não existe, logo não existe a permissão de acessar o
				// diretorio
				permission = false;
			}
		} else {
			String diretorio = nomeArquivo.substring(0, nomeArquivo.lastIndexOf("/"));
			try {
				// verificar se no diretorio existe um arquivo chamado
				// access.txt
				fileInputStream = new FileInputStream(diretorio + "/access.txt");

				@SuppressWarnings("resource")
				Scanner scanner = new Scanner(new FileReader(diretorio + "/access.txt")).useDelimiter(";");

				String login = "";
				String senha = "";

				int i = 0;
				while (i < 2) {
					login = scanner.next();
					login = login.substring(login.lastIndexOf("=") + 1);
					i++;
					senha = scanner.next();
					senha = senha.substring(senha.lastIndexOf("=") + 1);
					i++;
				}

				if (login.equals(config.getLogin()) && senha.equals(config.getSenha())) {
					System.out.println("acesso correto");
					permission = true;
				} else {
					System.out.println("acesso INcorreto");
					permission = false;
				}
			} catch (FileNotFoundException e) {
				// Access não existe, logo não existe a permissão de acessar o
				// diretorio
				permission = false;
			}
		}
		return permission;
	}

	private void criarEEnviarResposta(DataOutputStream dataOutputStream, String nomeArquivo) {
		criarEEnviarResposta(dataOutputStream, nomeArquivo, "");
	}

	private void criarEEnviarResposta(DataOutputStream dataOutputStream, String nomeArquivo, String statusLine) {
		try {
			int numberOfBytes = 0; // numero de bytes enviados para gravar no
									// log
			Boolean arquivoExiste = true;
			FileInputStream fileInputStream = null;
			boolean permission = true;
			try {
				fileInputStream = new FileInputStream(nomeArquivo);
			} catch (FileNotFoundException e) {
				arquivoExiste = false;
			}

			// Construir a mensagem de resposta.
			String contentTypeLine = null;
			String entityBody = null;

			permission = verificaSeAcessoExiste(nomeArquivo, permission);

			// caso exista a permissao
			if (permission) {
				if (arquivoExiste) {
					if (statusLine == "") {
						statusLine = "HTTP/1.0 200 Ok" + CRLF;
					}
					contentTypeLine = "Content-type: " + //
							contentType(nomeArquivo)//
							+ CRLF;

				} else {// arquivo nao existe
					if (statusLine == "") {
						statusLine = "HTTP/1.0 404 Not Found" + CRLF;
					}
					contentTypeLine = "Content-type: " + //
							"text/html"//
							+ CRLF;
					entityBody = "<HTML>" + "<HEAD><TITLE>Not Found</TITLE></HEAD>"
							+ "<BODY> Ops! Pagina nao encontrada! Tente outra URL =) </BODY></HTML>";
				}
			} else {// nao existe a permissão
				entityBody = "<HTML><TITLE>Access of " + nomeArquivo.replace(".", "") + " denied</TITLE><BODY>"
						+ "Você não tem acesso a este diretorio.</BODY></HTML>";
				if (statusLine == "") {
					statusLine = "HTTP/1.0 403 Forbidden" + CRLF;
				}
				contentTypeLine = "Content-type: " + "text/html" + CRLF;
			}

			// Enviar a linha de status.
			dataOutputStream.writeBytes(statusLine);
			numberOfBytes = numberOfBytes + statusLine.getBytes().length;

			// Enviar a linha de tipo de conteÃºdo.
			dataOutputStream.writeBytes(contentTypeLine);
			numberOfBytes = numberOfBytes + contentTypeLine.getBytes().length;

			// Enviar uma linha em branco para indicar o fim das linhas de
			// cabeçalho.
			dataOutputStream.writeBytes(CRLF);
			numberOfBytes = numberOfBytes + CRLF.getBytes().length;

			// Enviar o corpo da entidade.
			if (arquivoExiste && permission) {
				try {
					numberOfBytes = numberOfBytes + enviarDados(fileInputStream, dataOutputStream);
				} catch (Exception e) {
					// Se der erro no envio, envia mensagem de erro
					dataOutputStream.writeBytes(entityBody);
					numberOfBytes = numberOfBytes + entityBody.getBytes().length;
				}
				fileInputStream.close();
			} else {
				// Se arquivo não existe, envia mensagem de erro
				dataOutputStream.writeBytes(entityBody);
				numberOfBytes = numberOfBytes + entityBody.getBytes().length;
			}

			// Grava o log da transação
			log.printLog(socket.getInetAddress().getHostAddress(), socket.getPort(), nomeArquivo, numberOfBytes);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private int enviarDados(FileInputStream fileInputStream, DataOutputStream dataOutputStream) throws Exception {
		// Construir um buffer de 1K para comportar os bytes no caminho para o
		// socket.
		int countBytes = 0; // conta o numero de bytes do arquivo a ser enviado
		byte[] buffer = new byte[1024];
		int bytes = 0;

		// Copiar o arquivo requisitado dentro da cadeia de saÃ­da do socket.
		while ((bytes = fileInputStream.read(buffer)) != -1) {
			countBytes = countBytes + bytes;
			dataOutputStream.write(buffer, 0, bytes);
		}
		return countBytes;
	}

	private String contentType(String nomeArquivo) {
		if (nomeArquivo.endsWith(".htm") || nomeArquivo.endsWith(".html")) {
			return "text/html";
		}
		if (nomeArquivo.endsWith(".gif")) {
			return "image/gif";
		}
		if (nomeArquivo.endsWith(".jpg") || nomeArquivo.endsWith(".jpeg")) {
			return "image/jpeg";
		}
		return "application/octet-stream";
	}

	private String obterNomeArquivo(String linhaRequisicao) {

		// Extrair o nome do arquivo a linha de requisição.
		StringTokenizer tokens = new StringTokenizer(linhaRequisicao);
		tokens.nextToken(); // pular o método, que deve ser GET

		String nomeArquivo = tokens.nextToken();

		// Acrescente um â€œ.â€� de modo que a requisição do arquivo esteja
		// dentro
		// do diretorio atual.
		nomeArquivo = "." + nomeArquivo;
		return nomeArquivo;
	}

	private String obterLinhasDeCabecalho(BufferedReader bufferedReader, String linhaRequisicao) throws IOException {
		String linhasDeCabecalho = null;
		while ((linhasDeCabecalho = bufferedReader.readLine()).length() != 0) {
			System.out.println(linhasDeCabecalho);
		}

		if (linhaRequisicao.contains("POST")) {
			// code to read the post payload data
			StringBuilder params = new StringBuilder();
			while (bufferedReader.ready()) {
				params.append((char) bufferedReader.read());
			}

			String loginStr = params.substring(0, params.indexOf("&"));
			System.out.println(loginStr);
			login = loginStr.substring(loginStr.indexOf("=") + 1);
			System.out.println(login);

			String senhaStr = params.substring(params.indexOf("&") + 1);
			System.out.println(senhaStr);
			senha = senhaStr.substring(senhaStr.indexOf("=") + 1);
			System.out.println(senha);

		}
		return linhasDeCabecalho;
	}

	private String obterLinhaRequisicaoHTTP(BufferedReader bufferedReader) throws IOException {
		String linhaRequisicao = bufferedReader.readLine();

		// Exibir a linha de requisição.
		System.out.println();
		System.out.println(linhaRequisicao);
		return linhaRequisicao;
	}

	private boolean isDirectory(String nomeArquivo) {
		String pattern = "\\..*\\..*";
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(nomeArquivo);

		return !m.find();
	}
}
