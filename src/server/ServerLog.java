package server;

import java.io.*;
import java.text.*;
import java.util.Date;

public class ServerLog {
	
	ServerLog(){
	
	}
	
	private static File file = new File("./log.html");
	private static FileOutputStream fop;
	private static PrintWriter pw;
	
	public void startLog(){
		try{
			fop = new FileOutputStream(file, true);
			// if file doesn't exists, then create it
			if (!file.exists()) {
				file.createNewFile();
				pw = new PrintWriter(fop);
				pw.println("<HTML><BODY>");
				pw.flush();
				pw.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Erro log linha 23");
		}
	}
	public void printLog(String endOrigem, int portOrigem, String nomeArquivo, int bytes){
		pw = new PrintWriter (fop);
		pw.println("------Inicio Conexao------" + "</BR>");
		pw.println("Endereco de Origem: " + endOrigem + "</BR>");
		pw.println("Porta de Origem: " + portOrigem + "</BR>");
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		pw.println("Horario da requisicao: " + dateFormat.format(date) + "</BR>"); //2014/08/06 15:59:48
		pw.println("Conteudo requisitado: " + nomeArquivo + "</BR>");
		pw.println("Numero de Bytes enviados: " + bytes + "</BR>");
		pw.println("------Fim Conexao------" + "</BR>");
		
		//fop.write(contentInBytes);
		//fop.flush();
		//fop.close();
		//pw.println("</HTML></BODY>");
		pw.flush();
		pw.close();
	}
	public void finishLog(){
		pw = new PrintWriter (fop);
		//pw.println("</HTML></BODY>");
		pw.flush();
		pw.close();
		try {
			fop.flush();
			fop.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}