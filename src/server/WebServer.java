package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public final class WebServer {

	// Processar a requisição de serviço HTTP em um laço infinito.
	public static void main(String[] args) {
		try {
			// Ajustar o número da porta.
			int porta = 6789;

			// inicia o servidor
			new WebServer(porta).executa();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private int porta;

	public WebServer(int porta) {
		this.porta = porta;
	}

	public void executa() throws IOException {

		// Estabelecer o socket de escuta.
		@SuppressWarnings("resource")
		ServerSocket servidor = new ServerSocket(this.porta);
		System.out.println("Porta " + porta + " aberta!");

		Thread thread = null;

		// Processar a requisição de serviço HTTP em um laço infinito.
		while (true) {

			// Escutar requisição de conexão TCP
			Socket cliente = servidor.accept();

			System.out.println("Nova conex�o com a requisicao HTTP " + cliente.getInetAddress().getHostAddress());

			// Construir um objeto para processar a mensagem de requisição
			// HTTP.
			HttpRequest httpRequest = new HttpRequest(cliente);

			// Criar um novo thread para processar a requisição.
			thread = new Thread(httpRequest);

			// Iniciar o thread.
			thread.start();
		}
	}

}
